import React from 'react';
import { FlatList, Button, Alert, Platform, StyleSheet, View, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';;
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import ProductItemFlatList from '../../components/shop/ProductItemFlatList';
import Colors from '../../constants/Colors';
import * as productsActions from '../../store/actions/products-action';
import CustomHeaderButton from '../../components/UI-utils/CustomHeaderButtons';

const UserProductsScreen = ({ route, navigation }) => {
    const userProducts = useSelector(state => state.productsState.userProducts);
    const dispatch = useDispatch();


    const editProductHandler = id => {
        navigation.navigate('EditProductScreen', { productId: id });
    };

    const deleteHandler = (id) => {
        Alert.alert('Are you sure?', 'Do you really want to delete this item?', [
            { text: 'No', style: 'default' },
            {
                text: 'Yes',
                style: 'destructive',
                onPress: () => {
                    dispatch(productsActions.deleteProduct(id));
                }
            }
        ]);
    };

    React.useLayoutEffect(() => {
        navigation.setOptions({
              headerRight: () => ( 
                <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                    <Item title='Create' iconName={Platform.OS === 'android' ? 'md-add' : 'ios-add'} onPress={()=> { editProductHandler(null)} }/>
                 </HeaderButtons>
              )

        });
    }, [userProducts])

    if (userProducts.length === 0) {
        return (
            <View style={styles.noProductsContainer}>
                <Text style={styles.noProductsText}>No User Products to Show</Text>
            </View>
        );
    }
    return (
        <FlatList
            data={userProducts}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <ProductItemFlatList
                    image={itemData.item.imageUrl}
                    title={itemData.item.title}
                    price={itemData.item.price}
                    onSelect={() => { editProductHandler(itemData.item.id); }}
                >
                    {/* These buttons will be handle using 'props.children' in  ProductItemFlatList*/}
                    <Button color={Colors.primary} title="Edit" onPress={() => { editProductHandler(itemData.item.id); }} />
                    <Button color={Colors.primary} title="Create" onPress={() => { editProductHandler(null); }} />
                    <Button
                        color={Colors.primary}
                        title="Delete"
                        onPress={() => {
                            deleteHandler(itemData.item.id)
                        }}
                    />
                </ProductItemFlatList>
            )}
        />
    );
};

const styles = StyleSheet.create({
    noProductsContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    noProductsText: {
        fontFamily: 'opens-sans-bold',
        fontSize: 16
    }
})
export default UserProductsScreen;

import React, { useState, useReducer, useCallback, useEffect } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Button,
  Alert,
  KeyboardAvoidingView,
  ActivityIndicator
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import * as productsActions from '../../store/actions/products-action';
import FORM_INPUT_UPDATE from '../../constants/FormInputs';
import Input from '../../components/UI-utils/Inputs';
import Colors from '../../constants/Colors';

const formReducer = (state, action) => { // This DOES NOT nothing to be with REDUX, this a 'useReducer' hook of REACT
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = { ...state.inputValues, [action.input]: action.value };

    const updatedValidities = { ...state.inputsValidities, [action.input]: action.isValid };

    let updatedFormIsValid = true;
    for (const key in updatedValidities) { // if one of the inputs 'formIsValid' is false then formIsValid = false 
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputsValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state
}
const EditProductScreen = ({ route, navigation }) => {
  const { productId } = route.params;
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  let editedProduct = useSelector(state =>
    state.productsState.userProducts.find(prod => prod.id === productId) // if we already have the id then the form will used to Edit, else to create a produc
  );


  useEffect(() => { // its better use this hook at the begin of code.. to handle the change of the dependencies (array param)
    if (error) {
      Alert.alert('An error occurred!', error, [{ text: 'Okay' }]);
    }
  }, [error]);

  const [formState, dispatchFormState] = useReducer(formReducer, {

    // these are the Form inputs initial state
    inputValues: {

      title: editedProduct ? editedProduct.title : '',
      price: editedProduct ? editedProduct.price : '',
      imageUrl: editedProduct ? editedProduct.imageUrl : '',
      description: editedProduct ? editedProduct.description : ''

    }, inputsValidities: {

      title: editedProduct ? true : false,
      price: editedProduct ? true : false,
      imageUrl: editedProduct ? true : false,
      description: editedProduct ? true : false

    }, formIsValid: editedProduct ? true : false
  });

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );

  const submitHandler = useCallback(async () => { // To be sure that this function will be recreated every time the comp. renders
    if (!formState.formIsValid) {
      Alert.alert('Wrong input!', 'Please check the errors in the form.', [
        { text: 'Okay' }
      ]);
      return;
    }
    setError(null);
    setIsLoading(true)
    try {
    if (editedProduct) {
      await dispatch(
        productsActions.updateProduct(
          productId,
          formState.inputValues.title,
          formState.inputValues.description,
          formState.inputValues.imageUrl,
          +formState.inputValues.price)
      );
    } else {
      await dispatch(
        productsActions.createProduct(
          formState.inputValues.title,
          formState.inputValues.description,
          formState.inputValues.imageUrl,
          +formState.inputValues.price) // '+' to parse strings to numbers
      );
    }
    navigation.goBack();
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);

  }, [dispatch, productId, formState]); // be sure that the function  will be available/recreated when one of the values change


  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  return (
    <KeyboardAvoidingView // avoiding that keyboard overlaps the form
      style={{ flex: 1 }}
      behavior="padding"
      keyboardVerticalOffset={100}
    >
      <ScrollView>
        <View style={styles.form}>
          <View style={styles.formControl}>
            <Input
              id="title"
              label="Title"
              errorText="Please enter a valid title!"
              keyboardType="default"
              autoCapitalize="sentences"
              autoCorrect
              returnKeyType="next"
              onInputChange={inputChangeHandler}
              initialValue={editedProduct ? editedProduct.title : ''}
              initiallyValid={!!editedProduct}
              required
            />
          </View>
          <View style={styles.formControl}>
            <Input
              id="imageUrl"
              label="Image Url"
              errorText="Please enter a valid image url!"
              keyboardType="default"
              returnKeyType="next"
              onInputChange={inputChangeHandler}
              initialValue={editedProduct ? editedProduct.imageUrl : ''}
              initiallyValid={!!editedProduct}
              required
            />
          </View>

          <View style={styles.formControl}>
            <Input
              id="price"
              label="Price"
              errorText="Please enter a valid price!"
              keyboardType="decimal-pad"
              returnKeyType="next"
              onInputChange={inputChangeHandler}
              initialValue={editedProduct ? String(editedProduct.price) : ''}
              initiallyValid={!!editedProduct}  // !! if its TRUE
              required
              min={0.1}
            />
          </View>

          <View style={styles.formControl}>
            <Input
              id="description"
              label="Description"
              errorText="Please enter a valid description!"
              keyboardType="default"
              autoCapitalize="sentences"
              autoCorrect
              multiline
              numberOfLines={3}
              onInputChange={inputChangeHandler}
              initialValue={editedProduct ? editedProduct.description : ''}
              initiallyValid={!!editedProduct}  // !! if its TRUE
              required
              minLength={5}
            />
          </View>

          <View style={[styles.formControl, styles.buttonContainer]}>
            <Button style={styles.button} title='Save' onPress={() => submitHandler()}></Button>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};


const styles = StyleSheet.create({
  form: {
    margin: 20
  },
  buttonContainer: {
    marginTop: 30
  },
  button: {
    fontSize: 14,
    fontFamily: 'opens-sans-bold'
  }
});

export default EditProductScreen;
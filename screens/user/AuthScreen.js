import React, { useState, useReducer, useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { View, StyleSheet, KeyboardAvoidingView, ScrollView, Button, ActivityIndicator, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import Input from '../../components/UI-utils/Inputs';
import Card from '../../components/UI-utils/Card'
import Colors from '../../constants/Colors';
import * as authActions from '../../store/actions/auth-actions';

// ************************ This DOES NOT have anything to be with REDUX, this a 'useReducer' hook of REACT ******************************//
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        const updatedValidities = {
            ...state.inputValidities,
            [action.input]: action.isValid
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidities) {
            updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidities: updatedValidities,
            inputValues: updatedValues
        };
    }
    return state;
};
//********************************* */

export const AuthScreen = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const [isSignup, setIsSignup] = useState(false);
    const [areSubmitbuttonsAvailable, setAreSubmitbuttonsAvailable] = useState(false);
    const dispatch = useDispatch();
    const { navigation } = props;
    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            email: '',
            password: ''
        },
        inputValidities: {
            email: false,
            password: false
        },
        formIsValid: false
    });


    const inputChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity) => {
        dispatchFormState({
            type: FORM_INPUT_UPDATE,
            value: inputValue,
            isValid: inputValidity,
            input: inputIdentifier
        });
    },
        [dispatchFormState]
    );

    useEffect(() => {
        if (error) {
          Alert.alert('An Error Occurred!', error, [{ text: 'Okay' }]);
        }
      }, [error])


      const authHandler = async () => {
        let action;
        if (isSignup) {
          action = authActions.signup(
            formState.inputValues.email,
            formState.inputValues.password
          );
        } else {
          action = authActions.login(
            formState.inputValues.email,
            formState.inputValues.password
          );
        }
        setError(null);
        setIsLoading(true);
        try {
          let httpResp = await dispatch(action); // the action calling to fetch http function generates a promisse
          let sucessMss = 'The user has been created';
          setIsLoading(false);
          
          if (httpResp && isSignup){
            Alert.alert('Great!', sucessMss, [{ text: 'Okay' }]);
          }
          else if (httpResp && !isSignup) {
            props.navigation.navigate('ProductsOverviewScreen');
          }
          
        } catch (err) {
          setError(err.message);
          setIsLoading(false);
        }
      };

      React.useLayoutEffect(() => {
        navigation.setOptions({
          headerLeft: () => null, // removing default back button
        });
      })

    return (
        <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={15} style={styles.screen}>
            <LinearGradient colors={[Colors.loginGradient1, Colors.loginGradient2]} style={styles.gradient}>
                <Card style={styles.authContainer}>
                    <ScrollView>
                        <Input
                            id='email'
                            label='Email'
                            keyboardType='email-address'
                            required
                            email
                            autoCapitalize='none'
                            errorText='Please enter a valid Email address.'
                            onInputChange={inputChangeHandler}
                            initialValue='johnfuentes6@yopmail.com'
                        />
                        <Input
                            id='password'
                            label='Password'
                            keyboardType='default'
                            secureTextEntry={true} // it is not property/param for the custom comp. but remember that is rendering a RN input.
                            minLength={5}
                            required
                            autoCapitalize='none'
                            errorText='Please enter a valid Password.'
                            onInputChange={inputChangeHandler} // this a paramter as type function
                            initialValue='kurtco235'
                        />
                        <View style={styles.buttonContainer}>
                            {isLoading ? (
                                <ActivityIndicator size="small" color={Colors.primary} />
                            ) : (
                                    <Button
                                        title={isSignup ? 'Sign Up' : 'Login'}
                                        color={Colors.primary}
                                        onPress={authHandler}
                                    />
                                )}
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button
                                title={`Switch to ${isSignup ? 'Login' : 'Sign Up'}`}
                                color={Colors.accent}
                                onPress={() => {
                                    setIsSignup(prevState => !prevState);
                                }}
                            />
                        </View>
                    </ScrollView>
                </Card>
            </LinearGradient>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    authContainer: {
        width: '80%',
        maxWidth: 400, // in case of width is too much
        maxHeight: 400, // setting a default max height
        padding: 20
    },
    buttonContainer: {
        marginTop: 10
    }
});

export default AuthScreen;

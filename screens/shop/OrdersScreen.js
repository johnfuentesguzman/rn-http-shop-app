import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import  * as ordersActions from '../../store/actions/orders-actions';
import OrderItemFlatList from '../../components/shop/OrderItemFlatList';
import Colors from '../../constants/Colors';

const OrdersScreen = props => {
    const [isLoading, setIsLoading] = useState(false);
    const orders = useSelector(state => state.ordersState.orders);
    const dispatch = useDispatch();

    useEffect(() => {// this hook dosent allow to use "async": Using an async function makes the callback function return a Promise instead of a cleanup function. 
        setIsLoading(true);
        dispatch(ordersActions.fetchOrders()).then(() => {
            setIsLoading(false);
        }).catch((error) => {
            setIsLoading(false);
            Alert.alert('An error occurred!', error, [{ text: 'Okay' }]);
        });
    }, [dispatch]);


    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary} />
            </View>
        );
    }

    if (orders.length === 0) {
        return (
            <View style={styles.noOrdersContainer}>
                <Text style={styles.noOrdersText}>No Orders to Show</Text>
            </View>
        );
    }
    return (
        <FlatList
            data={orders}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <OrderItemFlatList
                    amount={itemData.item.totalAmount.toFixed(2)}
                    date={itemData.item.readableDate}
                    items={itemData.item.items}
                />
            )}
        />
    );
};

const styles = StyleSheet.create({
    centered: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    noOrdersContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    noOrdersText: {
        fontFamily: 'opens-sans-bold',
        fontSize: 16
    }
});
export default OrdersScreen;

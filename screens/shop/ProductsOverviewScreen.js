import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet, FlatList, Platform, Button, ActivityIndicator, Alert, AsyncStorage } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { Badge } from 'react-native-elements';

import ProductItemFlatList from '../../components/shop/ProductItemFlatList';
import * as cartActions from '../../store/actions/cart-actions';
import * as productsActions from '../../store/actions/products-action';
import Colors from '../../constants/Colors';
import CustomHeaderButton from '../../components/UI-utils/CustomHeaderButtons';
import  * as authActions  from '../../store/actions/auth-actions';

export const ProductsOverviewScreen = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [isRefreshing, setIsRefreshing] = useState(false);
  const { navigation } = props;
  const dispatch = useDispatch();
  const products = useSelector((state) => state.productsState.availableProducts);

  // this hook dosent allow to use "async": Using an async function makes the callback function return a Promise instead of a cleanup function.
  useEffect(() => {
    setIsLoading(true)
    if (error) {
      Alert.alert('An error occurred!', error, [{ text: 'Okay' }]);
      return;
    }
    loadProducts().then(() => {
      setIsLoading(false);
    });

  }, [dispatch, loadProducts, error]);

  const loadProducts = useCallback( async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(productsActions.fetchProducts()); // it returns a promisse¡
    } catch (error) {
      setError(error.message)
    }
    setIsRefreshing(false);

  },[dispatch,setIsLoading,setError]);


  const cartTotalProducts = useSelector(state => {
    const cartItems = [];
    for (const key in state.cartState.items) {
      cartItems.push({
        productId: key,
      });
    }
    return cartItems;
  });

  const selectItemHandler = (id, title) => {
    props.navigation.navigate('ProductDetailScreen', {
      productId: id,
      productTitle: title
    });
  };

  const logout = async () => {
    dispatch(authActions.logout());
    navigation.navigate('AuthScreen');
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => null, // removing default back button
      headerRight: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
          <Badge
            status="success"
            containerStyle={{ position: 'absolute', top: -13, right: 0 }}
            value={cartTotalProducts && cartTotalProducts.length}
          />
          <Item title='Logout' iconName={Platform.OS === 'android' ? 'md-log-out' : 'ios-log-out'} onPress={() => { logout()}} />
          <Item title='Orders' iconName={Platform.OS === 'android' ? 'md-cash' : 'ios-cash'} onPress={() => { navigation.navigate('OrdersScreen') }} />
          <Item title='Cart' iconName={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'} onPress={() => { navigation.navigate('CartScreen') }} />
        </HeaderButtons>
      )
    });
  }, [products, cartTotalProducts])

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && products.length === 0 ) {
    return (
      <View style={styles.centered}>
        <Text>No exist Products to show</Text>
        <Button title='Try Again'  color={Colors.primary} onPress={() => loadProducts()}></Button>
      </View>
    );
  }

  return (
    <FlatList
      onRefresh= {() => {
        loadProducts()
      }}
      refreshing={isRefreshing}
      data={products}
      keyExtractor={item => item.id}
      renderItem={itemData => (
        // attention: the params for the Item render function  MUST BE SPLITED ONE BY ONE  
        <ProductItemFlatList
          image={itemData.item.imageUrl}
          title={itemData.item.title}
          price={itemData.item.price}
          onSelect={() => {
            selectItemHandler(itemData.item.id, itemData.item.title);
          }}
        >
          {/* These buttons will be handle using 'props.children' in  ProductItemFlatList*/}
          <Button
            color={Colors.primary}
            title="View Details"
            onPress={() => {
              selectItemHandler(itemData.item.id, itemData.item.title);
            }}
          />
          <Button
            color={Colors.primary}
            title="To Cart"
            onPress={() => {
              dispatch(cartActions.addToCart(itemData.item));
            }}
          />
        </ProductItemFlatList>
      )}
    />
  )
}

const styles = StyleSheet.create({
  centered: { flex: 1, justifyContent: 'center', alignItems: 'center' }
});

export default ProductsOverviewScreen;
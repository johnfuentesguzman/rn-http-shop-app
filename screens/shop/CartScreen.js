import React, {useState , useCallback}from 'react';
import { View, Text, FlatList, Button, StyleSheet, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import * as cartActions from '../../store/actions/cart-actions';
import * as orderActions from '../../store/actions/orders-actions';

import Colors from '../../constants/Colors';
import CartItemFlatList from '../../components/shop/CartItemFlatList';

const CartScreen = props => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const cartTotalAmount = useSelector(state => state.cartState.totalAmount);
    
    const cartItems = useSelector(state => { // we need a array to pass to VIEW/return
        const transformedCartItems = [];
        for (const key in state.cartState.items) {
            transformedCartItems.push({
                productId: key,
                productTitle: state.cartState.items[key].productTitle,
                productPrice: state.cartState.items[key].productPrice,
                productQuantity: state.cartState.items[key].productQuantity,
                priceSummatory: state.cartState.items[key].priceSummatory
            });
        }
        return transformedCartItems.sort((a, b) => a.productId > b.productId ? 1 : -1 ); // showing data sorted by the latest
    });

    const createOrder = useCallback( async (cartItems, cartTotalAmount) => {
        setError(null);
        setIsLoading(true);
        try {
          await dispatch(orderActions.addOrder(cartItems, cartTotalAmount)) // it returns a promisse
        } catch (error) {
          setError(error.message)
        }
    
        setIsLoading(false);

    },[dispatch,setIsLoading,setError]);

    if (isLoading) {
        return (
          <View style={styles.centered}>
            <ActivityIndicator size="large" color={Colors.primary} />
          </View>
        );
    }

    if (!isLoading && cartItems.length === 0){
        return (
            <View style={styles.cartEmptyContainer}>
                <Text style={styles.cartEmptyText}>Your Cart is Empty</Text>
            </View>
        );
    }


    return (
        <View style={styles.screen}>
            <View style={styles.summary}>
                <Text style={styles.summaryText}>
                    Total:{' '}
                    <Text style={styles.amount}>${cartTotalAmount.toFixed(2)}</Text>
                </Text>
                <Button
                    color={Colors.accent}
                    title="Order Now"
                    disabled={cartItems.length === 0}
                    onPress={() => {
                        createOrder(cartItems, cartTotalAmount)
                    }}
                />
            </View>
            <FlatList
                data={cartItems}
                keyExtractor={item => item.productId}
                renderItem={itemData => (
                    <CartItemFlatList
                        quantity={itemData.item.productQuantity}
                        title={itemData.item.productTitle}
                        amount={itemData.item.priceSummatory}
                        deletable = 'true'
                        onRemove={() => {
                            dispatch(cartActions.removeFromCart(itemData.item.productId));
                        }}
                    />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    cartEmptyContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centered: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    cartEmptyText: {
        fontFamily: 'opens-sans-bold',
        fontSize: 16
    },
    screen: {
        margin: 20
    },
    summary: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
        padding: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white'
    },
    summaryText: {
        fontFamily: 'opens-sans-bold',
        fontSize: 18
    },
    amount: {
        color: Colors.primary
    }
});

export default CartScreen;

import React, { useEffect, useRef } from 'react';

import { useSelector } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import {
    View,
    ActivityIndicator,
    StyleSheet,
    AsyncStorage
  } from 'react-native';

import Colors from '../../constants/Colors';

  const navigationLogoutScreen = (props) => {
  const navRef = useRef();
  const isAuth = useSelector(state => !!state.authState.token); // if token is null then it retuns false and opposite is its true

  useEffect(() => { 
    if (!isAuth) { 
        NavigationActions.navigate({ routeName: 'AuthScreen' })
    //   navRef.current.dispatch(
    //     props.navigation.navigate({ routeName: 'AuthScreen' })
    //   );
    }
  }, [isAuth]);

  // you only have a way to get the navogiation properties into  components wich have beem declared into a wrapper (app.js, this case)
   // return <StartupSessionScreen ref={navRef} />; // get the navigation wrap from the components wrapper
    return (
        <View style={styles.screen}>
        </View>
      );
};

const styles = StyleSheet.create({
    screen: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
});
  
export default navigationLogoutScreen;

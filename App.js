import 'react-native-gesture-handler'; // must be ALWAYS FIRST
import React, {useState} from 'react';
import { Platform } from 'react-native'
import { NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createSwitchNavigator} from 'react-navigation'; // JUST THIS is still based in version 4x
import * as Font from 'expo-font';
import { AppLoading} from 'expo';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import { Provider }  from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk  from 'redux-thunk';

import ProductsOverviewScreen from './screens/shop/ProductsOverviewScreen';
import ProductDetailScreen from './screens/shop/ProductDetailScreen';
import EditProductScreen from './screens/user/EditProductScreen';
import AuthScreen from './screens/user/AuthScreen';
import StartupSessionScreen from './screens/session/StartupSessionScreen';
import NavigationLogoutScreen from './screens/session/navigationLogoutScreen'; // must be defined in uppercase becuase it gonna be wrapped out of stack navigation

import CartScreen from './screens/shop/CartScreen';
import OrdersScreen from './screens/shop/OrdersScreen';
import ProductsReducer from './store/reducers/products-reducer';
import cartReducer from './store/reducers/cart-reducer';
import ordersReducer from './store/reducers/orders-reducer';
import authReducer from './store/reducers/auth-reducer';

import Colors from './constants/Colors';
import UserProductsScreen from './screens/user/UserProductsScreen';
import { navigationRef } from './service/navigation-service';

const rootReducer = combineReducers({
  productsState: ProductsReducer,
  cartState: cartReducer,
  ordersState: ordersReducer,
  authState: authReducer
});

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(ReduxThunk),
  // other store enhancers if any
));

const fetchFonts=() => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'opens-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

const Stack = createStackNavigator();

function stackScreens({ navigation }) {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('tabPress', e => {
      e.preventDefault();
      let tabPressed = e.target.split("-")[0].toUpperCase();
      // I need to do this, because just setting the page as STACK navigitation , those page will have header
      switch (tabPressed) {
        case 'HOME':
          navigation.navigate('ProductsOverviewScreen');
        break;

        case 'ORDERS':
          navigation.navigate('OrdersScreen');
        break;

        case 'ADMIN':
          navigation.navigate('UserProductsScreen');
        break;
        
        default:
          navigation.navigate('ProductsOverviewScreen');
        break;
      }
    });

    return unsubscribe;
  }, [navigation]);
  return (
    <Provider store={store}>
        <Stack.Navigator>
          <Stack.Screen name="StartupSessionScreen" component={StartupSessionScreen}/>
          <Stack.Screen name="AuthScreen" component={AuthScreen} options={{ title: 'Login' }}/>
          <Stack.Screen name="ProductsOverviewScreen" component={ProductsOverviewScreen} options={{ title: 'All Products' }}/>
          <Stack.Screen name="ProductDetailScreen" component={ProductDetailScreen} options={{ title: 'Product Details' }}/>
          <Stack.Screen name="CartScreen" component={CartScreen} options={{ title: 'Cart' }}/>
          <Stack.Screen name="OrdersScreen" component={OrdersScreen} options={{ title: 'Your Orders' }}/>
          <Stack.Screen name="UserProductsScreen" component={UserProductsScreen} options={{ title: 'Your Products' }}/>
          <Stack.Screen name="EditProductScreen" component={EditProductScreen} options={{ title: 'Edit Products' }}/>
        </Stack.Navigator>
    </Provider>
  );
}

const Tab = createBottomTabNavigator();

export const App = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if(!fontsLoaded){
    return <AppLoading startAsync={fetchFonts} onFinish={()=> setFontsLoaded(true)}/>
  }

// navigationRef: set a ref to tell you that your app has finished mounting, and it gets all the navigation tree before to perform them
  return (
    <NavigationContainer  ref={ navigationRef } > 
      <Tab.Navigator
        initialRouteName="StartupSessionScreen" // main screen
        screenOptions={({ route }) => ({
          headerStyle: {
            backgroundColor: Colors.headerBackGroundColor,
          },
          headerTintColor: Colors.headerTintColor,
          headerTitleStyle: {
            fontWeight: Colors.headerfontWeight,
          },
        })}
      >
        <Tab.Screen name="Home" component={stackScreens}  options={{
          tabBarLabel: 'Home',
          tabBarIcon:() => (
            <Ionicons name='ios-home' size={28} color='black' />
          ),
        }} />
        <Tab.Screen name="Orders" component={stackScreens} options={{
          tabBarLabel: 'Orders',
          tabBarIcon:() => (
            <Ionicons name={Platform.OS === 'android' ? 'md-cash' : 'ios-cash'} size={28} color='black' />
          )
        }}/>
        <Tab.Screen name="Admin" component={stackScreens} options={{
          tabBarLabel: 'Admin',
          tabBarIcon:() => (
            <Ionicons name={Platform.OS === 'android' ? 'md-settings' : 'ios-settings'} size={28} color='black' />
          )
        }}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
} 

export default App;
export default {
    primary: '#C2185B',
    accent: '#FFC107',
    price: '#888',
    white: 'white',
    loginGradient1: '#373B44',
    loginGradient2: '#4286f4'

};
import moment from 'moment';

class Order {
    constructor(id, items, totalAmount, date) {
      this.id = id;
      this.items = items;
      this.totalAmount = totalAmount;
      this.date = date;
    }
    get readableDate() { // getter method / func
      return moment(this.date).format('MMMM Do YYYY, hh:mm'); // we need to use moment cause its supoorted for both platforms
    }
  }
  
  export default Order;
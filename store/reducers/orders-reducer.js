import { ADD_ORDER, FETCH_ORDERS } from '../actions/orders-actions';
import Order from '../../models/order';

const initialState = {
  orders: []
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORDERS:
      return {
        orders: action.orders
      };
    case ADD_ORDER:
      const newOrder = new Order(
        action.orderData.id,
        action.orderData.items,
        action.orderData.amount,
        action.orderData.date
      );
      return {
        ...state,
        orders: state.orders.concat(newOrder) // combine the array in just a single one
      };
    default:
      return state
  }
};

export default orderReducer;
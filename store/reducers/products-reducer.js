import PRODUCTS from '../../data/dummy-data';
import { DELETE_PRODUCT, UPDATE_PRODUCT, CREATE_PRODUCT, FETCH_PRODUCTS } from '../actions/products-action';
import Product from '../../models/product';


const initialState = {
  availableProducts: [],
  userProducts: []
};

const ProductsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS: 
    return {
      ...state,
      availableProducts: action.productData,
      userProducts: action.userProducts // products by user filtered in the action
    }

    case CREATE_PRODUCT:
      const newProduct = new Product(
        action.productData.id, // productId created by firebase
        action.productData.ownerId, // user dummy ID
        action.productData.title,
        action.productData.imageUrl,
        action.productData.description,
        action.productData.price
      );
      return {
        ...state,
        availableProducts: state.availableProducts.concat(newProduct), // JOIN the arrays
        userProducts: state.userProducts.concat(newProduct)
      };

    case UPDATE_PRODUCT:
      const productIndex = state.userProducts.findIndex(
        prod => prod.id === action.pid
      );
      const updatedProduct = new Product(
        action.pid,
        state.userProducts[productIndex].ownerId,
        action.productData.title,
        action.productData.imageUrl,
        action.productData.description,
        action.productData.price
      );
      const updatedUserProducts = [...state.userProducts]; // taking current products list
      updatedUserProducts[productIndex] = updatedProduct; // updatating the specific prod that the user has created.

      const availableProductIndex = state.availableProducts.findIndex(
        prod => prod.id === action.pid
      );

      const updatedAvailableProducts = [...state.availableProducts];
      updatedAvailableProducts[availableProductIndex] = updatedProduct; // updating the products to show in main page "products overview scren"
      return {
        ...state,
        availableProducts: updatedAvailableProducts,
        userProducts: updatedUserProducts
      };


    case DELETE_PRODUCT:
      return {
        ...state,
        userProducts: state.userProducts.filter(
          product => product.id !== action.pid
        ),
        availableProducts: state.availableProducts.filter(
          product => product.id !== action.pid
        )
      };
    default:
      return state;
  }
};

export default ProductsReducer;
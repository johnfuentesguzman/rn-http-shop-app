import { ADD_TO_CART, REMOVE_FROM_CART, ADD_ORDER  } from '../actions/cart-actions';
import { DELETE_PRODUCT } from '../actions/products-action';

import CartItem from '../../models/cart-item';


const initialState = {
    items: {},
    totalAmount: 0
};

const CartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            const { id, price, title } = action.productToAdd; // parameters in the action trigger
            let cartItem;

            if (state.items[id]) { // if already exist this product in cart
                cartItem = new CartItem( // Model
                    state.items[id].productQuantity + 1, // "productQuantity" ONCE is saved, it will use the items name in the MODEL
                    price,
                    title,
                    state.items[id].priceSummatory + price // "priceSummatory" ONCE is saved, it will use the items name in the MODEL
                );
            } else { // product fist time in Cart
                cartItem = new CartItem( // Model
                    1,
                    price,
                    title,
                    price
                );
            }
            return {
                ...state,
                items: { ...state.items, [id]: cartItem }, // it generates "id":{object}
                totalAmount: state.totalAmount + price
            }
            break;
        case REMOVE_FROM_CART:
            const selectedCartItem = state.items[action.pid];
            const currentQty = selectedCartItem.productQuantity;
            let updatedCartItems;
            if (currentQty > 1) {
                // need to reduce it, not erase it
                const updatedCartItem = new CartItem(
                    selectedCartItem.productQuantity - 1,
                    selectedCartItem.productPrice,
                    selectedCartItem.productTitle,
                    selectedCartItem.priceSummatory - selectedCartItem.productPrice
                );
                updatedCartItems = { ...state.items, [action.pid]: updatedCartItem };
            } else {
                updatedCartItems = { ...state.items };
                delete updatedCartItems[action.pid];
            }
            return {
                ...state,
                items: updatedCartItems,
                totalAmount: state.totalAmount - selectedCartItem.productPrice
            };  
        case ADD_ORDER: 
            return initialState;
        
        case DELETE_PRODUCT:
            if(!state.items [action.pid]){ // if the id product (param) does not exist. i not going to remove it
                return state;
            }
            const currentProductInCart = {...state.items};
            delete currentProductInCart[action.pid]; // removing item to remove by param
            const totalPrice_ItemToRemove = state.items[action.pid].priceSummatory;
            return {
                ...state,
                items: currentProductInCart,
                totalAmount:  state.totalAmount - totalPrice_ItemToRemove
            }
        default:
            return state;
    }
};

export default CartReducer;
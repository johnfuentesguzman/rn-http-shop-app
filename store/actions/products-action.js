import Data from '../../constants/Data';
import Product from '../../models/product'
import store from '../../App';

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';

export const fetchProducts = () => {
  return async (dispatch, getState) => { // this dispatch is for redux-thunk its the funcition that this middleware needs as parameter
    const userId = getState().authState.userId;
    // any async code you want!
    try {
      const response = await fetch(
        `${Data.firebaseHost}products.json`
      );
      if (!response.ok) {
        throw new Error('Opps, something was wrong')
      }

      const resData = await response.json();
      const loadedProducts = [];

      for (const key in resData) {
        loadedProducts.push(
          new Product(
            key,
            resData[key].ownerId,
            resData[key].title,
            resData[key].imageUrl,
            resData[key].description,
            resData[key].price
          )
        );

      }
      dispatch({ // thid dispatch is the normal way to triggers actions using REDUX != redux thunk middlelware in this function declaration
        type: FETCH_PRODUCTS,
        productData: loadedProducts,
        userProducts: loadedProducts.filter((prod) => prod.ownerId === userId)
      })
    } catch (err) {
      throw err;
    }
  }
};

export const createProduct = (title, description, imageUrl, price) => {
  try {
    return async (dispatch, getState) => { // getState Returns the current state tree of your application
      const token = getState().authState.token;
      const userId = getState().authState.userId;
      // any async http  call/code here !
      const response = await fetch(`${Data.firebaseHost}products.json?auth=${token}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          title,
          description,
          imageUrl,
          price,
          ownerId: userId
        })
      });
      if (!response.ok) {
        throw new Error('Opps, something was wrong')
      }

      const resData = await response.json();

      dispatch({
        type: CREATE_PRODUCT,
        productData: {
          id: resData.name,
          title,
          description,
          imageUrl,
          price,
          ownerId: userId
        }
      });
    };
  } catch (err) {
    throw err;
  }

};

export const updateProduct = (id, title, description, imageUrl, price) => {
  return async (dispatch, getState) => { // getState Returns the current state tree of your application
    const token = getState().authState.token;
    try {
      // any async http  call/code here !
      const response = await fetch(`${Data.firebaseHost}products/${id}.json?auth=${token}`, {
        method: 'PATCH', // It updates just the id what we want
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          title,
          description,
          imageUrl,
          price
        })
      });
      if (!response.ok) {
        throw new Error('Opps, something was wrong')
      }

      const resData = await response.json();
      dispatch({
        type: UPDATE_PRODUCT,
        pid: id,
        productData: {
          title,
          description,
          imageUrl,
          price
        }
      });

    } catch (error) {
      throw error;
    }
  };
};

export const deleteProduct = (productId) => {
  return async (dispatch, getState) => { // getState Returns the current state tree of your application
    const token = getState().authState.token;
    const response =  await fetch(
      `${Data.firebaseHost}products/${productId}.json?auth=${token}`,
      {
        method: 'DELETE'
      }
    );

    if (!response.ok) {
      throw new Error('Opps, something was wrong')
    }

    dispatch({ type: DELETE_PRODUCT, pid: productId });
  };
};
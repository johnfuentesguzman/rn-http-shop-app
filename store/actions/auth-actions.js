import {AsyncStorage} from 'react-native'; // save data in device storage, it works for both platforms

import Data from '../../constants/Data';
import { navigateToLogout } from '../../service/navigation-service';

//export const SIGNUP = 'SIGNUP';
//export const LOGIN = 'LOGIN';
export const AUTHENTICATE = 'AUTHENTICATE';
export const LOGOUT = 'LOGOUT';
let timer; 
export const authenticate = (userId, token, expiryTime) => {
  return dispatch => { // dispatch 2 o more actions is absolutely fine
    dispatch(setLogoutTimer(expiryTime));
    dispatch({ type: AUTHENTICATE, userId: userId, token: token });
  };
};

export const signup = (email, password) => {
  return async dispatch => {
    const response = await fetch(
      `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${Data.firebaseProjectApiKey}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password,
          returnSecureToken: true
        })
      }
    );

    if (!response.ok) {
      const errorResData = await response.json();
      const errorId = errorResData.error.message;
      let message = '';
      switch (errorId) {
        case 'EMAIL_EXISTS':
          message= 'This email exists already!';
        break;

        default:
          message = 'Invalid incredentials, Please try again';
        break;
      }
      throw new Error(message);
    }

    const resData = await response.json();
    console.log(resData);
    //dispatch({ type: SIGNUP, token: resData.idToken, userId: resData.localId});
    dispatch(authenticate(resData.localId, resData.idToken, parseInt(resData.expiresIn) * 1000)) ;

    const expirationDate = new Date(
      new Date().getTime() + parseInt(resData.expiresIn) * 1000  // using response data from firebase 'expiresIn' parse to miliseconds to know when exactly the token will be expired
    );
    saveDataToStorage(resData.idToken, resData.localId, expirationDate);

    return resData;
  };
};

export const login = (email, password) => {
  return async dispatch => {
    const response = await fetch(
      `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${Data.firebaseProjectApiKey}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password,
          returnSecureToken: true
        })
      }
    );

    if (!response.ok) {
      const errorResData = await response.json();
      const errorId = errorResData.error.message;
      let message = '';
      
      switch (errorId) {
        case 'MISSING_PASSWORD':
          message= 'Invalid Credentials'
        break;
        
        case 'EMAIL_NOT_FOUND':
          message = 'This email could not be found!';
        break;

        case 'INVALID_PASSWORD':
          message = 'This password is not valid!';
        break;

        default:
          message = 'Invalid incredentials, Please try again'
        break;
      }
      throw new Error(message);
    }

    const resData = await response.json();
    console.log(resData);
    //dispatch({ type: LOGIN, token: resData.idToken, userId: resData.localId});
    dispatch(authenticate(resData.localId, resData.idToken, parseInt(resData.expiresIn) * 1000) );

    const expirationDate = new Date(
      new Date().getTime() + parseInt(resData.expiresIn) * 1000  // using response data from firebase 'expiresIn' parse to miliseconds to know when exactly the token will be expired
    );
    saveDataToStorage(resData.idToken, resData.localId, expirationDate);

    return resData;
  };
};

export const logout = () =>{
  clearLogoutTimer();
  AsyncStorage.removeItem('userData'); // removing session data of session storage (device)
  navigateToLogout('AuthScreen');
  return {
    type: LOGOUT
  }
};

const clearLogoutTimer = () => {
  if (timer) {
    clearTimeout(timer); //  clears the timeout which has been set by setTimeout
  }
};

const setLogoutTimer = (expirationTime) => {
  return dispatch => {
    timer = setTimeout(() => { //when the token for session is expired then dispatch the action
      dispatch(logout());
    }, expirationTime );
  };
};

const saveDataToStorage = (token, userId, expirationDate) => { // save session data on device storage
  AsyncStorage.setItem(
    'userData',
    JSON.stringify({
      token: token,
      userId: userId,
      expiryDate: expirationDate.toISOString()
    })
  );
}
